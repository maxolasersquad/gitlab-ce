# Quick Start

To start building projects with GitLab CI a few steps need to be done.

## 1. Install GitLab and CI

First you need to have a working GitLab and GitLab CI instance.

You can omit this step if you use [GitLab.com](http://GitLab.com/).

## 2. Create repository on GitLab

Once you login to your GitLab account add a new repository where you will store your source code.
Push your application to that repository.

## 3. Add project to CI

The next part is to login to GitLab CI.
Point your browser to the URL you have set GitLab or use [gitlab.com/ci](http://gitlab.com/ci/).

On the first screen you will see a list of GitLab's projects that you have access to:

![Projects](projects.png)

Click **Add Project to CI**.
This will create a project in CI and authorize GitLab CI to fetch sources from GitLab.

> GitLab CI creates a unique token that is used to configure the GitLab CI service in GitLab.
> This token allows access to GitLab's repository and configures GitLab to trigger GitLab CI webhook on **Push events** and **Tag push events**.
> You can see the token by going to the project's Settings > Services > GitLab CI.
> The same token is assigned in GitLab CI settings of the project.

## 4. Create project's configuration - .gitlab-ci.yml

Next you have to define how your project will be built.
GitLab CI uses a [YAML](https://en.wikipedia.org/wiki/YAML) file to store build configurations.
You need to create `.gitlab-ci.yml` in the root directory of your repository:

```yaml
before_script:
  - bundle install

rspec:
  script:
    - bundle exec rspec

rubocop:
  script:
    - bundle exec rubocop
```

This is the simplest possible build configuration that will work for most Ruby applications:
1. Define two jobs, `rspec` and `rubocop`, with two different commands to be executed.
1. Before every job the commands defined by `before_script` will be executed.

The `.gitlab-ci.yml` file defines a set of jobs with constraints on how and when they should be run.
The jobs are defined as top-level elements with a name and always have to contain the `script`.
Jobs are used to create builds, which are then picked by [runners](../runners/README.md) and executed within the environment of the runner.
What is important is that each job is run independently from each other. 

For more information and complete `.gitlab-ci.yml` syntax, please check the [Configuring project (.gitlab-ci.yml)](../yaml/README.md).

## 5. Add the .gitlab-ci.yml file and push it to your repository

Once you created `.gitlab-ci.yml` you should add it to the git repository and push it to GitLab.

```bash
git add .gitlab-ci.yml
git commit
git push origin master
```

If you refresh the project's page on GitLab CI you will notice a new commit:

![](new_commit.png)

However the commit has status **pending** which means that the commit was not yet picked by runner.

## 6. Configure a runner

In GitLab CI, runners run your builds.
A runner is a machine (can be virtual, bare-metal or VPS) that picks up builds through the coordinator API of GitLab CI.

A runner can be specific to a certain project or serve any project in GitLab CI.
A runner that serves all projects is called a shared runner.
More information about different runner types can be found in [Configuring runner](../runners/README.md).

To check if you have runners assigned to your project go to **Runners**. There you will find information on how to setup project specific runners:

1. Install GitLab Runner software. Check out the [GitLab Runner](https://about.gitlab.com/gitlab-ci/#gitlab-runner) section to install it.
1. Specify the following URL during runner setup: https://gitlab.com/ci/
1. Use the following registration token during setup: TOKEN

If you do it correctly your runner should be shown under **Runners activated for this project**:

![](runners_activated.png)

### Shared runners

If you use [gitlab.com/ci](http://gitlab.com/ci/) you can use **Shared runners** provided by GitLab Inc.
These are special virtual machines that are run on GitLab's infrastructure that can build any project.
To enable **Shared runners** you have to go to **Runners** and click **Enable shared runners** for this project.

## 7. Check status of commit

If everything went OK and you go to commit, the status of the commit should change from **pending** to either **running**, **success** or **failed**.

![](commit_status.png)

You can click **Build ID** to view the build log for specific job.

## 8. Congratulations!

You managed to build your first project using GitLab CI.
You may need to tune your `.gitlab-ci.yml` file to implement a build plan for your project.
You can find a few examples of how it can be done on the [Examples](../examples/README.md) page.

GitLab CI also offers **the Lint** tool to verify the validity of your `.gitlab-ci.yml`, which can be useful for troubleshooting potential problems.
The Lint is available from the project's settings or by adding `/lint` to GitLab CI url.